require 'twilio-ruby'

# While there have been other things that could have been
# moved to their own classes, this one was chosen to separate
# due to the interaction with a third party API. That makes
# testing more difficult and brittle if in with the primary class.
# So, we'll pull this out so we can isolate that brittleness, allowing
# us to only run those tests when we absolutely need to.
class TwilioClient
  def initialize(sid, token)
    @client = Twilio::REST::Client.new(sid, token)
  end

  def get_numbers
    # We're going to take a bit of liberty here and assume we want numbers
    # with SMS and voice enabled and request those, specifically.
    begin
      result = @client.api.available_phone_numbers('US').local
              .list(sms_enabled: true, voice_enabled: true)
    rescue Twilio::REST::TwilioError => e
      return e.to_s
    end

    # The docs are a bit sparse about how to actually use the
    # Ruby client, specifically, so we're going with an example
    # that gives us what we need -- the numbers themselves.
    # Ideally, we'd just return the full number entries, but
    # that results in a list of "LocalInstance" objects, which are
    # oh-so-helpful and would require a bit more digging to find
    # a way to return the whole object in a more library-agnostic fashion.
    return result.map { |item| item.phone_number }
  end
end
