require 'prime'

class PhoneNumber

  def initialize(client)
    @client = client
  end

  def get_numbers
    entries = @client.get_numbers

    # A primitive way of stripping the "+1" off of the number.
    # This assumes we're getting all US numbers from the
    # response. This would need refactored if it were expanded
    # beyond the US, but phone numbers are...well...
    # we'll just leave this here:
    # https://github.com/googlei18n/libphonenumber/blob/master/FALSEHOODS.md
    return entries.map{ |item| item[2, 10].to_i }
  end

  # We're going to assume that Ruby's built in Prime class and
  # the algorithm it uses is good enough for the stated purpose.
  # If it's not, we're either a) using the wrong tool for the job
  # and should consider a more math-friendly language to do this,
  # or b) barking up the wrong tree entirely and should revisit
  # the "prime number" requirement to begin with.
  def find_primes(numbers)
    return numbers.select { |number| Prime.prime?(number) }
  end

  # In a larger/more ideal system, this would probably be better
  # refactored out to a service class, so that this class could
  # output in a more agnostic fashion, which would allow things
  # like outputting to a file *and* a database *and* sending a
  # message to a messaging system.
  def write_file(content:, file: 'output.txt', mode: 'w+')
    File.open(file, mode) do |f|
      unless content.respond_to?(:each)
        unless f.each_line.any?{ |line| line.include?(content.to_s) }
          f.puts(content)
          return true
        end
        break
      end

      written = false;
      content.each { |item|
        unless f.each_line.any?{ |line| line.include?(item.to_s) }
          f.puts(item)
          written = true
        end
      }
      return written
    end
  end
end
