Welcome to the prime-phone-number-finder project!

The prompt can be found in `prompt.md`, in case you're not familiar with it.

## Running The Application

1. Copy `config/env.yml.example` to `config/env.yml` and add your Twilio SID and token to the appropriate lines (I don't like storing API credentials in source control, and the phone end point requires the live credentials, so...)
2. Run `ruby fetch_numbers.rb` in the root folder.

The app will then print out its progress. If it finds one or more prime numbers, it will print it out into `output.txt`. If it doesn't find any, or if what it has found is already logged, it will let you know. It will continue this until there are 10 (default) or more entries in the file.

The application behaves this way by default, because the information in the prompt is a bit unclear as to whether the script should retrieve one number and stop (and then can be run subsequent times until the desired number has been achieved), or whether it should build a list of about 10 before stopping.

To account for this unclarity, and to help deal with the fact that finding unique prime numbers is at the mercy of what Twilio sends us, there is an option `-c` or `--count` which can be used to change the number of entries it finds before stopping.

## Following The Code

Config = `./config`  
App = `./lib`  
Tests = `./spec`

Both the spec and app files contain several notes providing information on things like lines of thinking, assumptions, and compromises made for the given conditions (and what might have been done differently given different conditions).
