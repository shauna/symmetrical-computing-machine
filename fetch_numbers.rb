require_relative './lib/phone_number'
require_relative './lib/twilio_client'
require 'optparse'
require 'envyable'
Envyable.load('config/env.yml')

@count = 10

OptionParser.new do |parser|
  parser.on('-c COUNT', '--count COUNT', 'The number of lines to generate.') do |v|
    @count = v.to_i
  end
end.parse!

@client = TwilioClient.new(ENV['TWILIO_SID'], ENV['TWILIO_TOKEN'])
@phone = PhoneNumber.new(@client)

until File.open('output.txt') { |f| f.count } >= @count
  puts 'Getting numbers...'
  numbers = @phone.get_numbers
  puts 'Finding primes...'
  primes = @phone.find_primes(numbers)
  if primes.first
    puts 'Writing to file...'
    written = @phone.write_file(content: primes, mode: 'a+')
    puts 'Found primes already logged! Trying again.' unless written
  else
    puts 'No primes found, trying again'
  end
end

puts 'Done!'
