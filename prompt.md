The Exercise

We’re sharing an exercise with you, so we can explore how you work and understand how you approach building software.

Twilio is a communications platform for adding things like programmable SMS and voice messaging to software applications.  To enable this communication, we first need to acquire a phone number from Twilio.  For arbitrary reasons, we require all of our phone numbers to be prime.

Write a PhoneNumber class capable of retrieving a unique, prime number available for purchase from Twilio and writing it to a file.  Then write a script uses the class some number of times, resulting in a file of unique prime phone numbers.

Functionality checklist:

    Each phone number must be a 10-digit prime (e.g. without the '+1')

    The script must be executable from a single command (e.g. 'ruby fetch_primes.rb')

    The script should be able to fetch and store at least 10 unique prime numbers.


Our goal is to not take more than a few hours of your time. If you have time, because testing is important, write some tests too. When you’re done, return your work in a way that we can easily run the program.

While we’re primarily a Ruby on Rails shop, we don’t necessarily expect you to be a Ruby expert. Use the language where you can demonstrate your best, just be sure it’s easy for us to make sure it works well.



Ideally, we’ll have your response within a few business days. After receiving your response, we’ll let you know within a few business days what next steps will be. Please reach out if you have any questions before or during the exercise.
