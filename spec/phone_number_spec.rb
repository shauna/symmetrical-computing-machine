require 'spec_helper'
require 'fakefs/safe'
require_relative '../lib/phone_number'

# Write a PhoneNumber class...
RSpec.describe PhoneNumber do
  subject(:script) { PhoneNumber.new({}) }

  context 'is a list of numbers' do
    # For arbitrary reasons, we require all of our phone numbers to be prime.
    it 'filters primes' do
      primes = [
        4093082899,
        9576890767,
        3628273133,
        2860486313
      ]
      other = [
        1234567890,
        2463677643,
        6145787244,
        3308978926
      ]

      expect(script.find_primes(primes + other)).to eq primes
    end
  end

  # ...and writing it to a file.
  # Depending on the exact needs, it may be better to mock the
  # file writing in another fashion. For brevity, we're going to
  # just isolate our tests with a fake filesystem.
  context 'is writing to file' do
    it 'outputs array to file' do
      FakeFS.with_fresh do
        numbers = [
          4093082899,
          9576890767
        ]
        name = 'test.txt'

        script.write_file(content: numbers, file: name)

        expect(File.read(name)).to include(numbers[0].to_s)
        expect(File.read(name)).to include(numbers[1].to_s)
      end
    end

    it 'outputs single item to file' do
      FakeFS.with_fresh do
        content = 4093082899
        name = 'test.txt'

        script.write_file(content: content, file: name)

        expect(File.read(name)).to include(content.to_s)
      end
    end

    it 'appends to file' do
      FakeFS.with_fresh do
        numbers = [
          9576890767
        ]
        to_add = 4093082899
        name = 'test.txt'

        script.write_file(content: numbers, file: name)
        script.write_file(content: to_add, file: name, mode: 'a+')

        expect(File.read(name)).to include(numbers[0].to_s)
        expect(File.read(name)).to include(to_add.to_s)
      end
    end
  end

  # We don't want to depend on the actual Twilio client
  # if we can help it, especially since we don't care here
  # about the inner workings of the client. The important
  # part is the we handle the data it returns. So, we'll
  # mock the TwilioClient class and test its inner
  # workings elsewhere.
  # Dependency injection for the win!
  subject(:script) { PhoneNumber.new(instance_double('TwilioClient', :get_numbers => [
    '+19576890767',
    '+18578654892',
    '+17395837586',
    '+19385749385',
    '+12859372057'
  ])) }

  # To enable this communication, we first need to acquire a phone number from Twilio.
  it 'fetches 10-digit numbers' do
    expect(script.get_numbers).to eq([9576890767, 8578654892, 7395837586, 9385749385, 2859372057])
  end
end
