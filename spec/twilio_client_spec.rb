require 'spec_helper'
require 'envyable'
require_relative '../lib/twilio_client'

# For simplicity's sake, we're going the env file route for environment
# variables. This makes knowledge transfer a bit easier in this situation.
# In cases where we're in control of the deployment environments, these
# may be stored in server-level environment variables or other more
# appropriate "Ruby way."
Envyable.load('config/env.yml')

RSpec.describe TwilioClient do
  subject(:client) { TwilioClient.new(ENV['TWILIO_SID'], ENV['TWILIO_TOKEN']) }

  # In a larger system with more potential services for getting
  # numbers, we'd want to be able to inject any of the clients,
  # so this expectation would be a shared one to ensure they
  # behave predictably when used. Since this is the only one we're
  # doing, and for the sake of brevity, we're just going to put it here
  # and pretend that it's part of a larer ecosystem of clients.
  # In languages with such support, we would create an interface
  # to create a contract that concrete classes would then implement.
  it { is_expected.to respond_to(:get_numbers) }

  # Due to how we ended up returning the numbers, there's not a
  # *whole* lot we can do to check this, especially since we don't
  # (and can't, feasibly) care about the exact data, so long as we
  # have it in the format we're expecting.
  it 'gets a set of numbers from Twilio' do
    result = client.get_numbers
    expect(result.is_a? Enumerable).to eq true
    # Matches an 11-digit number with a "+" in front (ie +11234567890)
    expect(result[0]).to match(/\A\+\d{11}\z/)
  end
end
